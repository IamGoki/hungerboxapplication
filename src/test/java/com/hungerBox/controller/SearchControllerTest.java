package com.hungerBox.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hungerbox.controller.SearchController;
import com.hungerbox.model.Item;
import com.hungerbox.service.SearchService;

@RunWith(MockitoJUnitRunner.class)
public class SearchControllerTest {

	@InjectMocks
	SearchController searchController;

	@Mock
	SearchService searchService;

	static Item search = null;

	@BeforeClass
	public static void setUp() {
		search = new Item();

	}

	@Test
	public void testsearchItemForPositive() {
		List<Item> items = new ArrayList();
		Item item = new Item();
		item.setItemId(1L);
		item.setName("chicken");
		item.setUnitPrice(200.0);
		item.setItemType("biryani");
		String name = "";
		ResponseEntity<List<Item>> item1 = searchController.getItemByNameLike(name);
		Assert.assertNotNull(item1);
		Assert.assertEquals( HttpStatus.OK,item1.getStatusCode());

	}

	@Test
	public void testsearchItemForNegative() {
		List<Item> items = new ArrayList();
		Item item = new Item();
		item.setItemId(-1L);
		item.setName("chicken");
		item.setUnitPrice(300.0);
		item.setItemType("biryani");
		String name = "";
		ResponseEntity<List<Item>> item1 = searchController.getItemByNameLike(name);
		Assert.assertNotNull(item1);
		Assert.assertEquals(HttpStatus.OK,item1.getStatusCode());

	}

	@AfterClass
	public static void tearDown() {
		search = null;
	}
}
