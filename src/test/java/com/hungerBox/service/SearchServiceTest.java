package com.hungerBox.service;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import com.hungerbox.exception.ItemNotFoundException;
import com.hungerbox.model.Item;
import com.hungerbox.repository.ItemRepository;
import com.hungerbox.service.SearchServiceImpl;

import junit.framework.Assert;


@RunWith(MockitoJUnitRunner.Silent.class)
public class SearchServiceTest {
	@InjectMocks
	SearchServiceImpl searchServiceImpl;

	@Mock
	ItemRepository searchRepository;

	@Test(expected = ItemNotFoundException.class)
	public void testGetItemByNameStartingWithPositive() {

		List<Item> items = new ArrayList();
		
	
		Item item = new Item();
		item.setItemId(1L);
		item.setName("chicken");
		item.setUnitPrice(300.0);
		item.setItemType("biryani");
		String name = "";
items.add(item);
		Mockito.when(searchRepository.getItemByNameLike(name)).thenReturn(items);
		 List<Item> itemss=	searchServiceImpl.getItemByNameStartingWith(item.getName());
		 
		Assert.assertEquals(items, itemss);

	}

	@Test
    public void TestviewItemByNameForPositive()  {
        
		List<Item> items =new ArrayList<>();
		Item item = new Item();
		item.setItemId(1l);
		item.setItemType("southIndian");
		item.setName("chicken");
        item.setUnitPrice(200.00);
        item.setItemDescription("spicy");
    
		items.add(item); 
        
        Mockito.when(searchRepository.getItemByNameLike(Mockito.anyString())).thenReturn(items);
        List<Item> result=searchServiceImpl.getItemByNameStartingWith(item.getName());
        assertNotNull(result);
        
    }

}
