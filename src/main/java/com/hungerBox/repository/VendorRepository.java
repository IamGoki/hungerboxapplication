package com.hungerbox.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hungerbox.model.Vendor;

public interface VendorRepository extends JpaRepository<Vendor, Long>{

}
