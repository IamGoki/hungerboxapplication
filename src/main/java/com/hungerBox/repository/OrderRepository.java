package com.hungerbox.repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.hungerbox.model.Order;


public interface OrderRepository extends JpaRepository<Order,Long>{

}
