package com.hungerbox.exception;

public class EmployeeNotFoundException extends RuntimeException {
	/*
	 * public EmployeeNotFoundException(String message) { super(message); }
	 */
	public EmployeeNotFoundException(String string) {
		super(string);
	}

}
