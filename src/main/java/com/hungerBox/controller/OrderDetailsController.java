package com.hungerbox.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hungerbox.dto.EmployeeOrder;
import com.hungerbox.service.OrderDetailsService;

/**
 * 
 * @author saikrishna
 *
 */

@RestController
public class OrderDetailsController {

	@Autowired
	OrderDetailsService orderDetailsService;
	
	/**
	 * 
	 * @param employeeId
	 * @return EmployeeOrder
	 */
	
 @GetMapping(value="/order")
 public ResponseEntity<EmployeeOrder> orderList(@RequestParam long employeeId){
	 EmployeeOrder orderItemList = orderDetailsService.orderList(employeeId);
	 return new ResponseEntity<>(orderItemList,HttpStatus.OK);
 }
}
