package com.hungerbox.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.hungerbox.model.Item;
import com.hungerbox.service.SearchService;

/**
 * 
 * @author chandhini
 * @version 1.0
 * @since 2020-06-18
 *
 */

@RestController
public class SearchController {
	Logger logger = LoggerFactory.getLogger(SearchController.class);

	@Autowired
	SearchService searchService;

	/**
	 * 
	 * @param name
	 * @return List<Item>
	 */

	@GetMapping(value = "/search/{search}")
	public ResponseEntity<List<Item>> getItemByNameLike(@PathVariable("search") String name) {

		List<Item> items = searchService.getItemByNameStartingWith(name);
		return new ResponseEntity<>(items, HttpStatus.OK);
	}

}
