package com.hungerbox.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hungerbox.dto.OrderDto;
import com.hungerbox.dto.ResponseDto;
import com.hungerbox.service.OrderService;
/**
 * 
 * @author saikrishna
 *
 */



@RestController
public class OrderController {
	Logger logger = LoggerFactory.getLogger(OrderController.class);
	 
	@Autowired
	OrderService orderService;
	
	/**
	 * 
	 * @param orderDto
	 * @return ResponseDto
	 */
	
	@PostMapping("/order")
	public ResponseEntity<ResponseDto> placeOrder(@RequestBody OrderDto orderDto)  {
		ResponseDto message =orderService.saveOrder(orderDto);
		logger.info("place order in OrderController");
		return new ResponseEntity<>(message, HttpStatus.OK);
	}

}
