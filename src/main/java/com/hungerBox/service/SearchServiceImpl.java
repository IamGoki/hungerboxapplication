package com.hungerbox.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hungerbox.exception.ItemNotFoundException;
import com.hungerbox.model.Item;
import com.hungerbox.repository.ItemRepository;

@Service
public class SearchServiceImpl implements SearchService {
	@Autowired
	ItemRepository searchRepository;

	@Override
	public List<Item> getItemByNameStartingWith(String name) {
		List<Item> items = searchRepository.getItemByNameLike("%" + name + "%");
		if (items.isEmpty()) {
			throw new ItemNotFoundException("given item not found");
		}
		return items;
	}

}
